# Generated by Django 2.1.1 on 2018-10-03 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal', '0003_auto_20181003_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='jadwalpribadi',
            name='tanggal',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='jadwalpribadi',
            name='waktu',
            field=models.CharField(default='', max_length=200),
        ),
    ]
