from django.shortcuts import render, HttpResponse
from .forms import jadwalForm
from .models import jadwalPribadi
# Create your views here.

def form(request):
    form = jadwalForm()
    all_entry = jadwalPribadi.objects.all()

    response = {'form' : form, 'database' : jadwalPribadi.objects.all()}
    return render(request, 'formJadwal.html', response)


response = {}
def addjadwal(request):

    if request.method == 'POST':
        form = jadwalForm(request.POST or None)
        if form.is_valid():
            response['name'] = request.POST['name']
            response['tempat'] = request.POST['tempat']
            response['kategori'] = request.POST['kategori']
            response['waktu'] = request.POST['waktu']
            response['tanggal'] = request.POST['tanggal']
            jadwal = jadwalPribadi(nama=response['name'], tempat=response['tempat'], kategori=response['kategori'], waktu=response['waktu'], tanggal=response['tanggal'] )
            jadwal.save()
            return render(request, 'addjadwal.html', response)

    else:
        return HttpResponse('<h1>TEST<h1>')

def hapus(request):
    jadwalPribadi.objects.all().delete()
    return render(request, 'hapus.html')
