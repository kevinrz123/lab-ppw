from django import forms
from .models import jadwalPribadi

class jadwalForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput({'class' : 'form-control'}))
    tempat = forms.CharField(max_length=200, widget=forms.TextInput({'class' : 'form-control'}))
    kategori = forms.CharField(max_length=50, widget=forms.TextInput({'class' : 'form-control'}))
    waktu = forms.CharField(widget=forms.TextInput({'class': 'form-control', 'type': 'time'}))
    tanggal = forms.CharField(widget=forms.TextInput({'class' : 'form-control', 'type' : 'date'}))


