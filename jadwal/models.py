from django.db import models


# Create your models here.
class jadwalPribadi(models.Model):
    nama = models.CharField(max_length=100, default='Anonymous')
    tempat = models.CharField(max_length=200, default='')
    kategori = models.CharField(max_length=50, default='Other')
    waktu = models.CharField(max_length=200, default='')
    tanggal = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.nama