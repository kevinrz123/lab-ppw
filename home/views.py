from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'profil.html')

def register(request):
    return render(request, 'register.html')

def kontak(request):
    return render(request, 'kontak.html')